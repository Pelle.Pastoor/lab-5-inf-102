package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        nodes.add(node);
    }

    @Override
    public void removeNode(V node) {
        nodes.remove(node);

        nodeToNode.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        if(!hasNode(u) || !hasNode(v)) throw new IllegalArgumentException("Can't add edge for non existing node.");

        addNode(u);
        addNode(v);

        Set<V> uEdges = nodeToNode.getOrDefault(u, new HashSet<>());
        Set<V> vEdges = nodeToNode.getOrDefault(v, new HashSet<>());

        uEdges.add(v);
        vEdges.add(u);

        nodeToNode.put(u, uEdges);
        nodeToNode.put(v, vEdges);
    }

    @Override
    public void removeEdge(V u, V v) {
        if(!hasNode(u) || !hasNode(v)) return;

        addNode(u);
        addNode(v);

        Set<V> uEdges = nodeToNode.getOrDefault(u, new HashSet<>());
        Set<V> vEdges = nodeToNode.getOrDefault(v, new HashSet<>());

        uEdges.remove(v);
        vEdges.remove(u);

        nodeToNode.put(u, uEdges);
        nodeToNode.put(v, vEdges);
    }

    @Override
    public boolean hasNode(V node) {
        return nodes.contains(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
