package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        Set<V> visited = new HashSet<>();
        Stack<V> q = new Stack<>();
        q.push(u);

        while (!q.isEmpty()){
            V n = q.pop();

            if(n.equals(v)) return true;

            if(!visited.contains(n)){
                visited.add(n);

                Set<V> nodes = new HashSet<>();

                try {
                    nodes = graph.getNeighbourhood(n);

                    q.addAll(nodes);
                } catch (NullPointerException e) {
                    continue;
                }
            }
        }
        
        return false;
    }

}
